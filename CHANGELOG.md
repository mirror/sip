## [0.5.0](https://gitea.com/jolheiser/sip/pulls?q=&type=all&state=closed&milestone=1311) - 2020-09-17

* ENHANCEMENTS
  * Refactor (#29)
  * Add release attachments (#26)
  * Update repo info (#24)
* BUILD
  * Update Gitea SDK (#23)


## [0.4.0](https://gitea.com/jolheiser/sip/pulls?q=&type=all&state=closed&milestone=1309) - 2020-09-12

* FEATURES
  * Add open functionality (#21)
* BUILD
  * Update modules (#20)

## [0.3.0](https://gitea.com/jolheiser/sip/pulls?q=&type=all&state=closed&milestone=1244) - 2020-09-03

* FEATURES
  * Add release support and CSV output (#16)
  * Add create repo command (#13)
* ENHANCEMENTS
  * Update Gitea SDK and other modules (#19)
* DOCS
  * Imp formatting (#17)


## [0.2.0](https://gitea.com/jolheiser/sip/pulls?q=&type=all&state=closed&milestone=1237) - 2020-02-26

* BUGFIXES
  * Fix PR head nil panic (#7)
  * Update Beaver and fix bugs (#6)
* ENHANCEMENTS
  * Add search filters (#8)
* BUILD
  * Add Drone and releases (#2)
