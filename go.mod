module go.jolheiser.com/sip

go 1.13

require (
	code.gitea.io/sdk/gitea v0.13.0
	github.com/AlecAivazis/survey/v2 v2.1.1
	github.com/BurntSushi/toml v0.3.1
	github.com/alecthomas/chroma v0.8.0 // indirect
	github.com/charmbracelet/glamour v0.2.0
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/dlclark/regexp2 v1.2.1 // indirect
	github.com/hashicorp/go-version v1.2.1 // indirect
	github.com/huandu/xstrings v1.3.2
	github.com/kyokomi/emoji v1.5.1
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/microcosm-cc/bluemonday v1.0.4 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/muesli/termenv v0.7.2 // indirect
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/urfave/cli/v2 v2.2.0
	github.com/yuin/goldmark v1.2.1 // indirect
	go.jolheiser.com/beaver v1.0.2
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
	golang.org/x/net v0.0.0-20200904194848-62affa334b73 // indirect
	golang.org/x/sys v0.0.0-20200909081042-eff7692f9009 // indirect
	golang.org/x/text v0.3.3 // indirect
)
