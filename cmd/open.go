package cmd

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"go.jolheiser.com/sip/flag"

	"github.com/skratchdot/open-golang/open"
	"github.com/urfave/cli/v2"
)

var Open = cli.Command{
	Name:    "open",
	Aliases: []string{"o"},
	Usage:   "Open a repository or issue/pull request",
	Action:  doOpen,
}

func doOpen(ctx *cli.Context) error {
	if ctx.NArg() == 0 {
		return open.Run(flag.FullURL())
	}

	arg := ctx.Args().First()

	// Check if issue or PR
	issue, err := strconv.ParseInt(arg, 10, 64)
	if err == nil {
		return open.Run(fmt.Sprintf("%s/issues/%d", flag.FullURL(), issue))
	}

	// Check if overriding repository (jolheiser/sip)
	ownerRepoIssue := strings.Split(arg, "/")
	if len(ownerRepoIssue) == 2 {
		return open.Run(fmt.Sprintf("%s/%s", flag.FullURL(), arg))
	}

	// Check if both? (jolheiser/sip/1234)
	if len(ownerRepoIssue) == 3 {
		return open.Run(fmt.Sprintf("%s/%s/%s/issues/%s", flag.URL, ownerRepoIssue[0], ownerRepoIssue[1], ownerRepoIssue[2]))
	}

	return errors.New("unknown argument: leave blank to open current repo, pass issue/PR as #1234, or override repo as owner/repo")
}
