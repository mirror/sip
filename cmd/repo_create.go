package cmd

import (
	"strings"

	"code.gitea.io/sdk/gitea"
	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/beaver/color"
)

var RepoCreate = cli.Command{
	Name:   "create",
	Usage:  "Create a new repository",
	Action: doRepoCreate,
}

func doRepoCreate(_ *cli.Context) error {
	client, err := getClient(true)
	if err != nil {
		return err
	}

	questions := []*survey.Question{
		{
			Name:     "name",
			Prompt:   &survey.Input{Message: "Name"},
			Validate: survey.Required,
		},
		{
			Name:   "description",
			Prompt: &survey.Input{Message: "Description"},
		},
		{
			Name:     "private",
			Prompt:   &survey.Confirm{Message: "Private", Default: true},
			Validate: survey.Required,
		},
		{
			Name:     "auto",
			Prompt:   &survey.Confirm{Message: "Auto-initialize", Default: false},
			Validate: survey.Required,
		},
	}
	answers := struct {
		Name        string
		Description string
		Private     bool
		Auto        bool
	}{}

	if err := survey.Ask(questions, &answers); err != nil {
		return err
	}

	opts := gitea.CreateRepoOption{
		Name:        answers.Name,
		Description: answers.Description,
		Private:     answers.Private,
		AutoInit:    answers.Auto,
	}

	if answers.Auto {
		auto := autoOpts(client)
		opts.Gitignores = strings.Join(auto.Gitignores, ",")
		opts.License = auto.License
		opts.Readme = auto.Readme
	}

	repo, _, err := client.CreateRepo(opts)
	if err != nil {
		return err
	}

	beaver.Infof("Created new repo at %s", color.FgMagenta.Format(repo.HTMLURL))
	return nil
}

type autoInit struct {
	Gitignores []string
	License    string
	Readme     string
}

func autoOpts(client *gitea.Client) autoInit {
	// FIXME query gitignores, licenses, and readme when available in SDK
	_ = client
	return autoInit{Readme: "Default"}
}
