package cmd

import (
	"github.com/urfave/cli/v2"
)

var Pulls = cli.Command{
	Name:    "pulls",
	Aliases: []string{"pull", "pr"},
	Usage:   "Commands for interacting with pull requests",
	Action:  doPullsSearch,
	Subcommands: []*cli.Command{
		&PullsCreate,
		&PullsStatus,
		&PullsCheckout,
	},
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:  "csv",
			Usage: "Output results to a CSV file at `PATH`",
		},
	},
}

func doPullsSearch(ctx *cli.Context) error {
	if _, err := issuesSearch(ctx, true); err != nil {
		return err
	}
	return nil
}
