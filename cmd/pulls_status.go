package cmd

import (
	"fmt"
	"strconv"

	"go.jolheiser.com/sip/flag"
	"go.jolheiser.com/sip/git"
	"go.jolheiser.com/sip/sdk"

	"code.gitea.io/sdk/gitea"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver/color"
)

var PullsStatus = cli.Command{
	Name:   "status",
	Usage:  "View the status of a pull request",
	Action: doPullStatus,
}

func doPullStatus(_ *cli.Context) error {
	client, err := getClient(false)
	if err != nil {
		return err
	}

	head := fmt.Sprintf("%s:%s", flag.Origin.Owner, git.Branch())

	pulls, err := sdk.GetPulls(client, flag.Upstream.Owner, flag.Upstream.Repo, gitea.ListPullRequestsOptions{State: "all"})
	if err != nil {
		return err
	}

	var pr *gitea.PullRequest
	for _, pull := range pulls {
		if pull.Head == nil {
			continue
		}
		compare := fmt.Sprintf("%s:%s", pull.Head.Repository.Owner.UserName, pull.Head.Name)
		if compare == head {
			pr = pull
			break
		}
	}

	if pr == nil {
		return fmt.Errorf("no pull request found with head target %s", color.New(color.FgMagenta).Format(head))
	}

	index := color.New(color.FgCyan).Format("#" + strconv.Itoa(int(pr.Index)))
	title := color.New(color.FgYellow).Format(pr.Title)
	state := color.New(color.FgGreen).Format("[open]")
	if pr.HasMerged {
		state = color.New(color.FgMagenta).Format("[merged]")
	} else if pr.State == gitea.StateClosed {
		state = color.New(color.FgRed).Format("[closed]")
	}
	lbls := make([]string, len(pr.Labels))
	for idx, label := range pr.Labels {
		lbls[idx] = label.Name
	}
	fmt.Println(index, title, state)
	fmt.Println(color.New(color.FgCyan).Format(fmt.Sprintf("%d comments", pr.Comments)))
	fmt.Println(color.New(color.FgYellow).Format(pr.HTMLURL))

	return nil
}
