package cmd

import (
	"go.jolheiser.com/sip/config"

	"github.com/AlecAivazis/survey/v2"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Config = cli.Command{
	Name:    "config",
	Aliases: []string{"cfg"},
	Usage:   "Modify Sip config",
	Action:  doConfig,
	Subcommands: []*cli.Command{
		{
			Name:   "origin",
			Usage:  "Specify default origin name",
			Action: doConfigOrigin,
		},
		{
			Name:   "upstream",
			Usage:  "Specify default upstream name",
			Action: doConfigUpstream,
		},
	},
}

func doConfig(ctx *cli.Context) error {
	if err := doConfigOrigin(ctx); err != nil {
		return err
	}
	return doConfigUpstream(ctx)
}

func doConfigOrigin(_ *cli.Context) error {
	question := &survey.Input{
		Message: "Default origin name",
		Default: "origin",
	}
	var answer string
	if err := survey.AskOne(question, &answer); err != nil {
		return err
	}

	config.Origin = answer
	if err := config.Save(); err != nil {
		return err
	}

	beaver.Info("Default origin saved!")
	return nil
}

func doConfigUpstream(_ *cli.Context) error {
	question := &survey.Input{
		Message: "Default upstream name",
		Default: "upstream",
	}
	var answer string
	if err := survey.AskOne(question, &answer); err != nil {
		return err
	}

	config.Upstream = answer
	if err := config.Save(); err != nil {
		return err
	}

	beaver.Info("Default upstream saved!")
	return nil
}
