package cmd

import (
	"strconv"

	"go.jolheiser.com/sip/flag"
	"go.jolheiser.com/sip/sdk"

	"code.gitea.io/sdk/gitea"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/beaver/color"
)

var Repo = cli.Command{
	Name:   "repo",
	Usage:  "Commands for interacting with a Gitea repository",
	Action: doRepo,
	Subcommands: []*cli.Command{
		&RepoCreate,
	},
}

func doRepo(_ *cli.Context) error {
	client, err := getClient(false)
	if err != nil {
		return err
	}

	repo, _, err := client.GetRepo(flag.Owner, flag.Repo)
	if err != nil {
		return err
	}

	issues, err := sdk.GetIssues(client, flag.Owner, flag.Repo, gitea.ListIssueOption{State: "open"})
	if err != nil {
		return err
	}

	var issueCount, pullCount int
	for _, issue := range issues {
		if issue.PullRequest != nil {
			pullCount++
			continue
		}
		issueCount++
	}

	yellow := color.New(color.FgYellow)
	beaver.Infof("Repository:  %s", yellow.Format(repo.FullName))
	beaver.Infof("URL:         %s", yellow.Format(repo.HTMLURL))
	beaver.Info()
	beaver.Infof("Open Issues: %s", yellow.Format(strconv.Itoa(issueCount)))
	beaver.Infof("Open Pulls:  %s", yellow.Format(strconv.Itoa(pullCount)))
	beaver.Info()
	beaver.Infof("Forks:       %s", yellow.Format(strconv.Itoa(repo.Forks)))
	beaver.Infof("Stars:       %s", yellow.Format(strconv.Itoa(repo.Stars)))
	beaver.Infof("Watchers:    %s", yellow.Format(strconv.Itoa(repo.Watchers)))

	return nil
}
