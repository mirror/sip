# NAME

Sip - Command line tool to interact with Gitea

# SYNOPSIS

Sip

```
[--origin]=[value]
[--owner|-o]=[value]
[--repo|-r]=[value]
[--token|-t]=[value]
[--upstream]=[value]
[--url|-u]=[value]
```

**Usage**:

```
Sip [GLOBAL OPTIONS] command [COMMAND OPTIONS] [ARGUMENTS...]
```

# GLOBAL OPTIONS

**--origin**="": The origin remote

**--owner, -o**="": The owner to target (default: jolheiser)

**--repo, -r**="": The repo to target (default: sip)

**--token, -t**="": The access token to use (by name)

**--upstream**="": The upstream remote

**--url, -u**="": The base URL to the Gitea instance (default: https://gitea.com)


# COMMANDS

## config, cfg

Modify Sip config

### origin

Specify default origin name

### upstream

Specify default upstream name

## tokens, token

Manage access tokens

### add, create

Add a new access token

### remove, delete

Remove access tokens

## repo

Commands for interacting with a Gitea repository

### create

Create a new repository

## issues, issue

Commands for interacting with issues

**--csv**="": Output results to a CSV file at `PATH`

### create, new

Create a new issue

## pulls, pull, pr

Commands for interacting with pull requests

**--csv**="": Output results to a CSV file at `PATH`

### create, new

Create a new pull request

### status

View the status of a pull request

### checkout

Checkout a pull request for testing

## releases, release

Commands for interacting with releases

**--csv**="": Output results to a CSV file at `PATH`

### create, new

Create a new release

### attach

Attach files to a release

#### remove, delete, rm

Remove attachments from a release

## open, o

Open a repository or issue/pull request
