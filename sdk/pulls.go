package sdk

import (
	"code.gitea.io/sdk/gitea"
)

// GetPulls returns all matching PRs from a Gitea instance
func GetPulls(client *gitea.Client, owner, repo string, opts gitea.ListPullRequestsOptions) ([]*gitea.PullRequest, error) {
	pulls := make([]*gitea.PullRequest, 0)
	p := 1
	for {
		opts.Page = p
		list, _, err := client.ListRepoPullRequests(owner, repo, opts)
		if err != nil {
			return pulls, err
		}
		p++
		pulls = append(pulls, list...)

		if len(list) == 0 {
			break
		}
	}
	return pulls, nil
}
