package sdk

import (
	"strconv"
	"testing"
)

func TestIssueFilter(t *testing.T) {

	tt := []struct {
		Query  string
		Result *IssueFilter
	}{
		// Grouped in fives to make searching failed tests easier
		{"test query", &IssueFilter{Query: "test query"}},
		{"test query author:jolheiser", &IssueFilter{Query: "test query", Author: "jolheiser"}},
		{"test query author:jolheiser author:zeripath", &IssueFilter{Query: "test query", Author: "zeripath"}},
		{"test is:open query", &IssueFilter{Query: "test query", State: Open}},
		{"is:closed", &IssueFilter{State: Closed}},

		{"is:closed is:merged", &IssueFilter{State: Merged}},
		{"is:merged test author:jolheiser query", &IssueFilter{Query: "test query", State: Merged, Author: "jolheiser"}},
		{"label:bug", &IssueFilter{Labels: []string{"bug"}}},
		{"label:bug label:feature", &IssueFilter{Labels: []string{"bug", "feature"}}},
		{"label:bug author:jolheiser test is:open query is:closed", &IssueFilter{Query: "test query", Author: "jolheiser", State: Closed, Labels: []string{"bug"}}},

		{"is:closed milestone:0.1.0", &IssueFilter{State: Closed, Milestone: "0.1.0"}},
		{"milestone:v1.0.0 keyword", &IssueFilter{Query: "keyword", Milestone: "v1.0.0"}},
		{`label:"a bug" keyword`, &IssueFilter{Query: "keyword", Labels: []string{"a bug"}}},
	}

	for idx, tc := range tt {
		t.Run(strconv.Itoa(idx+1), func(t *testing.T) {
			filter := NewIssueFilter(tc.Query)
			if tc.Result.Query != filter.Query {
				t.Fail()
			}
			if tc.Result.State != filter.State {
				t.Fail()
			}
			if tc.Result.Author != filter.Author {
				t.Fail()
			}
			if len(tc.Result.Labels) != len(filter.Labels) {
				t.Fail()
			}
		})
	}
}
